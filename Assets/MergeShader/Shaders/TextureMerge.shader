﻿Shader "Wayward/TextureMergeShader"
{
	Properties
	{
	    //Left and right textures to merge
		_LeftTex ("Left Texture", 2D) = "white" {}
		_RightTex ("Right Texture", 2D) = "white" {}
		
		//Transitioning value between the two textures
		_SeperationValue ("Seperation Value", Range(0.0, 1.0)) = 0.5

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

            //Parameters
			sampler2D _LeftTex;
			sampler2D _RightTex;
			float4 _LeftTex_ST;
			float _SeperationValue;
			
			//Default vertex funtion
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _LeftTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
			    //Painting left texture first
				fixed4 col = tex2D(_LeftTex, i.uv);

				//Overriding the Left texture with the right texture using the slider value and uv (change x to another y to change merge axis from vertical to horizontal)
				//max(sign(i.uv.x - _SeperationValue) for mathematical 'if' statement
				//For more info about mathematical conditionals: http://theorangeduck.com/page/avoiding-shader-conditionals
				col = lerp(tex2D(_LeftTex, i.uv), tex2D(_RightTex, i.uv) , max(sign(i.uv.x - _SeperationValue), 0.0));

				return col;
			}
			ENDCG
		}
	}
}