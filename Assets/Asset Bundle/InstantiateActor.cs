﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class InstantiateActor : MonoBehaviour
{
    public string bundleName;
    public string modelName;
    public bool loadMe;

    public void Update()
    {
        if (loadMe == true)
        {
            loadMe = false;
            InstanceFromBundle();
        }
    }

    public void InstanceFromBundle()
    {
        string pth = Path.Combine(Application.streamingAssetsPath, bundleName);
        var myLoadedAssetBundle = AssetBundle.LoadFromFile(pth);
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }
        var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(modelName);
        Instantiate(prefab);
    }
}