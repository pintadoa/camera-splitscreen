﻿using UnityEngine;

public class DynamicSplitScreen : MonoBehaviour
{
    [Tooltip("Mask for camera player 1 ")]
    public Texture2D maskP1;
    [Tooltip("Mask for camera player 2 ")]
    public Texture2D maskP2;

    [Tooltip("Camera  player 1")]
    public Camera CameraP1;
    [Tooltip("Camera  player 2")]
    public Camera CameraP2;

    [Tooltip("Transform of player 1")]
    public Transform positionP1;
    [Tooltip("Transform of player 2")]
    public Transform positionP2;

    [Tooltip("Distance players can separate on Z axis")]
    [Range(0.1f,100)]
    public float zPlayerThreshold;
    [Tooltip("Distance players can separate on X axis")]
    [Range(0.1f, 100)]
    public float xPlayerThreshold;
    public float viewportRect = 0.47f;
    [Tooltip("Speed the cameras lerp together")]
    [Range(0.001f, 1)]
    public float  cameraLerpSpeed = 0.08f;

    private enum ScreenState { Full, SplitVertical, SplitHorizontal };
    private ScreenState currentScreen;

    // Calculated cameras midpoint position
    private Vector3 midPoint;
    private bool lerpToMidpoint;
    private bool finishedLerp;

    // Cameras original position
    private Vector3 originalP1Position;
    private Vector3 originalP2Position;

    private void Start()
    {
        currentScreen = ScreenState.SplitVertical;
        lerpToMidpoint = true;

        // Get the position of where the camera is placed
        originalP1Position = CameraP1.transform.localPosition;
        originalP2Position = CameraP2.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        float currentXThreshold = Vector3.Distance(new Vector3(positionP1.transform.position.x, 0.0f), new Vector3(positionP2.transform.position.x, 0.0f));
        float currentZThreshold = Vector3.Distance(new Vector3(0.0f,0.0f, positionP1.transform.position.z), new Vector3(0.0f,0.0f, positionP2.transform.position.z));

        float percentageX = currentXThreshold / xPlayerThreshold;
        float percentageZ = currentZThreshold / zPlayerThreshold;

        // Check if the players are inside the view threshold
        if (currentXThreshold < xPlayerThreshold && currentZThreshold < zPlayerThreshold)
        {
            
            if(currentScreen != ScreenState.Full)
            {
                finishedLerp = false;
            }

            // Reset X & Y viewport values
            CameraP1.rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
            CameraP2.rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);

            lerpToMidpoint = true;
            midPoint = new Vector3((positionP1.position.x + positionP2.position.x) / 2,
                                                CameraP1.transform.position.y,
                                                (positionP1.position.z + positionP2.position.z)/2);

            currentScreen = ScreenState.Full;
        }
        else
        {
            lerpToMidpoint = false;
            if (percentageX > percentageZ)
            {
                if (currentXThreshold > xPlayerThreshold)
                {
                    // Check if display type has switched
                    if (currentScreen != ScreenState.SplitVertical)
                    {
                        finishedLerp = false;

                        DefineRect(positionP1.position.x, positionP2.position.x, true);
                    }

                    // Check which camera goes Left or Right on screen
                    if (positionP1.position.x > positionP2.position.x)
                    {
                        GenerateMask(maskP1, maskP2, true);
                    }
                    else
                    {
                        GenerateMask(maskP2, maskP1, true);
                    }
                }
                currentScreen = ScreenState.SplitVertical;
            }
            else
            {
                if (currentZThreshold > zPlayerThreshold)
                {
                    // Check if display type has switched
                    if (currentScreen != ScreenState.SplitHorizontal)
                    {
                        finishedLerp = false;

                        DefineRect(positionP1.position.z, positionP2.position.z, false);               
                    }

                    // Check which camera goes Up or Down on screen
                    if (positionP1.position.z > positionP2.position.z)
                    {
                        GenerateMask(maskP1, maskP2, false);
                    }
                    else
                    {
                        GenerateMask(maskP2, maskP1, false);
                    }
                }
                currentScreen = ScreenState.SplitHorizontal;
            }
        }
    }

    private void LateUpdate()
    {
        CameraLerp();
    }

    private void DefineRect(float p1Axis, float p2Axis, bool isVertical)
    {
        // Assign which side the camera is gonna render on X or Y
        float rectP1 = (p1Axis > p2Axis) ? viewportRect : -viewportRect;
        float rectP2 = (p1Axis > p2Axis) ? -viewportRect : viewportRect;

        // Assign the reccts to the respective cameras Viewport Rect
        CameraP1.rect = isVertical ? new Rect(rectP1, 0.0f, 1.0f, 1.0f) : new Rect(0.0f, rectP1, 1.0f, 1.0f);
        CameraP2.rect = isVertical ? new Rect(rectP2, 0.0f, 1.0f, 1.0f) : new Rect(0.0f, rectP2, 1.0f, 1.0f);
    }

    private void GenerateMask(Texture2D camera1, Texture2D camera2, bool isVertical)
    {
        // Get pixels from the texture
        Color32[] pixelsCamera1 = camera1.GetPixels32(0);
        Color32[] pixelsCamera2 = camera2.GetPixels32(0);

        // Loop over pixels creating the masks
        for (int y = (camera1.height - 1); y >= 0; y--)
        {
            for (int x = 0; x < camera1.width; x++)
            {
                // Check over which axis the pixel division will be made
                int slice = (isVertical) ? x : y;
                int screenSize = (isVertical) ? camera1.width / 2 : camera1.height / 2;
                if (slice < screenSize)
                {
                    pixelsCamera1[x + y * camera1.width] = Color32.Lerp(pixelsCamera1[x + y * camera1.width], Color.blue, 0.33f);
                    pixelsCamera2[x + y * camera2.width] = Color32.Lerp(pixelsCamera2[x + y * camera2.width], Color.clear, 0.33f);
                }
                else
                {
                    pixelsCamera1[x + y * camera1.width] = Color32.Lerp(pixelsCamera1[x + y * camera1.width], Color.clear, 0.33f);
                    pixelsCamera2[x + y * camera2.width] = Color32.Lerp(pixelsCamera2[x + y * camera2.width], Color.blue, 0.33f);
                }
            }
        }

        // Set the new generated arrays to the camera
        camera1.SetPixels32(pixelsCamera1, 0);
        camera2.SetPixels32(pixelsCamera2, 0);

        camera1.Apply();
        camera2.Apply();
    }

    private void CameraLerp()
    {
        if (lerpToMidpoint)
        {
            // Check if the cameras are at the same point
            if (Vector3.Distance(CameraP1.transform.position, midPoint) < 0.1f)
            {
                finishedLerp = true;
            }

            if (Vector3.Distance(CameraP1.transform.position, midPoint) > 0.1f && finishedLerp == false)
            {
                CameraP1.transform.position = Vector3.Lerp(CameraP1.transform.position, midPoint, cameraLerpSpeed);
                CameraP2.transform.position = Vector3.Lerp(CameraP2.transform.position, midPoint, cameraLerpSpeed);
            }
            else
            {
                CameraP1.transform.position = Vector3.Lerp(CameraP1.transform.position, midPoint, 1);
                CameraP2.transform.position = Vector3.Lerp(CameraP2.transform.position, midPoint, 1);
            }
        }
        else
        {
            if (Vector3.Distance(CameraP1.transform.localPosition, originalP1Position) > 0.1f)
            {
                CameraP1.transform.localPosition = Vector3.Lerp(CameraP1.transform.localPosition, originalP1Position, cameraLerpSpeed);
                CameraP2.transform.localPosition = Vector3.Lerp(CameraP2.transform.localPosition, originalP2Position, cameraLerpSpeed);
            }
        }
    }
}
